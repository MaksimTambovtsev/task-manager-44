package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IOwnerRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserOwnedService;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Optional;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@Nullable final R repository, @NotNull IConnectionService connection) {
        super(repository, connection);
    }

    @NotNull
    public abstract IOwnerRepository<M> getRepository(@NotNull EntityManager entityManager);

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IOwnerRepository<M> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IOwnerRepository<M> repository = getRepository(entityManager);
        try {
            @Nullable final M result = repository.findById(userId, id);
            return result;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IOwnerRepository<M> repository = getRepository(entityManager);
        try {
            return repository.getSize(userId);
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IOwnerRepository<M> repository = getRepository(entityManager);
        try {
            @Nullable final M result = repository.findById(userId, id);
            if (result == null) return;
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

}
