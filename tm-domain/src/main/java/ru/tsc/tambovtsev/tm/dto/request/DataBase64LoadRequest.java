package ru.tsc.tambovtsev.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(@Nullable String token) {
        super(token);
    }

}
