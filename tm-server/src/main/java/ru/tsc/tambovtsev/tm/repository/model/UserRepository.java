package ru.tsc.tambovtsev.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.IUserRepository;
import ru.tsc.tambovtsev.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    protected static final String TABLE_NAME = "User";

    public UserRepository(@NotNull final EntityManager connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public @Nullable User findById(@Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + TABLE_NAME + " WHERE ID = :id", User.class)
                .setParameter("id", id));
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        return getResult(entityManager
                .createQuery("FROM " + TABLE_NAME + " WHERE LOGIN = :login", User.class)
                .setParameter("login", login));
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        return getResult(entityManager
                .createQuery("FROM " + TABLE_NAME + " WHERE EMAIL = :email", User.class)
                .setParameter("email", email));
    }

    @Override
    @SneakyThrows
    public void lockUserById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE " + TABLE_NAME + " SET LOCKED = 1 WHERE ID = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @SneakyThrows
    public void unlockUserById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE " + TABLE_NAME + " SET LOCKED = 0 WHERE ID = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @SneakyThrows
    public void updateUser(@NotNull User user) {
        entityManager
                .merge(user);
    }

    @Override
    public void addAll(@NotNull Collection<User> models) {
        entityManager
                .persist(models);
    }

    @Override
    @SneakyThrows
    public void removeByLogin(final String login) {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME + " WHERE LOGIN = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<User> findAllUser() {
        return entityManager
                .createQuery("FROM " + TABLE_NAME, User.class).getResultList();
    }

    @Override
    public void clearUser() {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME).executeUpdate();
    }

}
