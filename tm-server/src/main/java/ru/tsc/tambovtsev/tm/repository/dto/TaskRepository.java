package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<TaskDTO> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "TaskDTO";

    public TaskRepository(@NotNull final EntityManager connection) {
        super(connection);
    }

    @Override
    public @Nullable TaskDTO findById(@Nullable String userId, @Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id AND USER_ID = :userId", TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId));
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        return entityManager
                .createQuery("FROM " + getTableName() + " ORDER BY " + sort.name() + " ASC", TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public @Nullable TaskDTO findById(@Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id", TaskDTO.class)
                .setParameter("id", id));
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull TaskDTO task) {
        entityManager
                .merge(task);
    }

    @Override
    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId AND PROJECT_ID = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public @Nullable List<TaskDTO> findAllTask() {
        return entityManager
                .createQuery("FROM " + getTableName(), TaskDTO.class)
                .getResultList();
    }

    @Override
    public void clearTask() {
        entityManager
                .createQuery("DELETE FROM " + getTableName())
                .executeUpdate();
    }

    @Override
    public void addAll(@NotNull Collection<TaskDTO> models) {
        entityManager
                .persist(models);
    }

}
