package ru.tsc.tambovtsev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TM_USER")
public class User extends AbstractEntity {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "LOGIN")
    private String login;

    @Nullable
    @Column(name = "PASSWORD_HASH")
    private String passwordHash;

    @Nullable
    @Column(name = "EMAIL")
    private String email;

    @Nullable
    @Column(name = "FIRST_NAME")
    private String firstName;

    @Nullable
    @Column(name = "LAST_NAME")
    private String lastName;

    @Nullable
    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @NotNull
    @Column(name = "ROLE")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "LOCKED")
    private Boolean locked = false;

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Boolean isLocked() {
        return locked;
    }

    public User(@Nullable String login, @Nullable String passwordHash, @Nullable String email, @Nullable String firstName, @Nullable String lastName) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
