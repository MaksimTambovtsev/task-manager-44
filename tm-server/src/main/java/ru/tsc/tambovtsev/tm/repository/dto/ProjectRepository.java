package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<ProjectDTO> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "ProjectDTO";

    public ProjectRepository(@NotNull final EntityManager connection) {
        super(connection);
    }

    @Override
    public @Nullable ProjectDTO findById(@Nullable String userId, @Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id AND USER_ID = :userId", ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId));
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        return entityManager
                .createQuery("FROM " + TABLE_NAME + " ORDER BY " + sort.name() + " ASC", ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public @Nullable ProjectDTO findById(@Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + TABLE_NAME + " WHERE ID = :id", ProjectDTO.class)
                .setParameter("id", id));
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull ProjectDTO project) {
        entityManager
                .merge(project);
    }

    @Override
    public @Nullable List<ProjectDTO> findAllProject() {
        return entityManager
                .createQuery("FROM " + TABLE_NAME, ProjectDTO.class)
                .getResultList();
    }

    @Override
    public void removeByIdProject(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME + " WHERE USER_ID = :userId AND ID = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void clearProject() {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME)
                .executeUpdate();
    }

    @Override
    public void addAll(@NotNull Collection<ProjectDTO> models) {
        entityManager
                .persist(models);
    }

}
