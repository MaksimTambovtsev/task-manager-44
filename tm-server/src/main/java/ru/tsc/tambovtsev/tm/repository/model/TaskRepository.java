package ru.tsc.tambovtsev.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "Task";

    public TaskRepository(@NotNull final EntityManager connection) {
        super(connection);
    }

    @Override
    public @Nullable Task findById(@Nullable String userId, @Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id AND USER_ID = :userId", Task.class)
                .setParameter("id", id)
                .setParameter("userId", userId));
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String userId, @Nullable Sort sort) {
        return entityManager
                .createQuery("FROM " + getTableName() + " ORDER BY " + sort.name() + " ASC", Task.class)
                .getResultList();
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public @Nullable Task findById(@Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id", Task.class)
                .setParameter("id", id));
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull Task task) {
        entityManager
                .merge(task);
    }

    @Override
    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId AND PROJECT_ID = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public @Nullable List<Task> findAllTask() {
        return entityManager
                .createQuery("FROM " + getTableName(), Task.class)
                .getResultList();
    }

    @Override
    public void clearTask() {
        entityManager
                .createQuery("DELETE FROM " + getTableName())
                .executeUpdate();
    }

    @Override
    public void addAll(@NotNull Collection<Task> models) {
        entityManager
                .persist(models);
    }

}
